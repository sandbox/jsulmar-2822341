<?php

/**
 * @file
 * Contains \Drupal\yarm\Controller\YarmTestController.
 */

namespace Drupal\yarm\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\SafeMarkup;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\StreamWrapper\PrivateStream;

//require_once realpath(__DIR__ . "/../../xlib/yarm-lib/php/YarmUpload.php");
//require_once DRUPAL_ROOT . "/sites/all/libraries/yarm-lib/php/YarmUpload.php";

$upload = DRUPAL_ROOT . "/sites/all/libraries/yarm-lib/php/YarmUpload.php";
if (file_exists($upload))
  include $upload;

class YarmTestController extends ControllerBase {

  public $config = [
    'usePrivate' => 0, //use the private file destination
    'uploadDirectory' => 'yarm_uploads'   //folder within destination
  ];

  /*
   * hello world test
   */

  public function hello() {
    return array(
      '#type' => 'markup',
      '#markup' => $this->t('yarm:  Hello, World!'),
    );
  }

  /*
   * present a test page to demonstrate basic scaffolding
   */

  public function scaffold() {
    $title = t('yarm - Basic Scaffolding Test');

    $build = [
      '#title' => $title,
      //this method renders output without using a template
      //'#type' => 'markup',
      //'#markup' => $this->t('yarm:Hello, World!'),
      //this uses a template
      '#theme' => 'test_scaffold',
      '#source_text' => SafeMarkup::checkPlain("this is the 'source_text' variable."),
    ];

    // Add a script. Pass the module name followed by the internal library 
    // name declared in libraries yml file.
    $build['#attached']['library'][] = 'yarm/yarm.test_scaffold';
    return $build;
  }

  /*
   * present a YarmRecorder test page
   */

  public function lib() {
    $build = [
      //use a twig template, declared in hook_theme()
      '#theme' => 'test_lib',
    ];

    // provide the upload url as a javascript arg
    $build['#attached']['drupalSettings']['yarm']['uploadUrl'] = self::getUploadUrl();

    // Add the required script libraries, declared in libraries yml file. 
    $build['#attached']['library'][] = 'yarm/yarm.yarm-lib';
    $build['#attached']['library'][] = 'yarm/yarm.test_lib';
    $build['#attached']['library'][] = 'yarm/yarm.jplayer';
    return $build;
  }

  /*
   * present a jPlayer test page
   */

  public function jplayer() {
    $build = [
      //use a twig template, declared in hook_theme()
      '#theme' => 'test_jplayer',
    ];

    // Add the required script libraries, declared in libraries yml file. 
    $build['#attached']['library'][] = 'yarm/yarm.jplayer';
    $build['#attached']['library'][] = 'yarm/yarm.test_jplayer';
    return $build;
  }

  /*
   * invoked by AJAX script to upload and process a new recording
   */

  public function upload() {

    //if configuration specifies the private file system, and if the private
    //path is set, use it.  Otherwise use the public file system.
    $uploadDestination = ($this->config['usePrivate'] && PrivateStream::basePath()) ? PrivateStream::basePath() //private path
        : \Drupal::service('file_system')->realpath(file_default_scheme() . "://")  //public path
    ;

    $response = new JsonResponse();
    $save_folder = $uploadDestination . "/" . $this->config['uploadDirectory'];
    $response->setData(
        \YarmUpload::fileCatch(
            false, //disable fileCatch method writing directly to output stream
            $save_folder    //destination path for files
        )
    );
    return $response;
  }

  //construct URL of the 'upload' service
  public static function getUploadUrl() {
    $request = Request::createFromGlobals();
    $server = $request->server->all();
    //example values:
    //    [REQUEST_SCHEME] => http
    //    [HTTP_HOST] => localhost
    //    [PHP_SELF] => /d8_sandbox/index.php
    return
//            $server['REQUEST_SCHEME'] . '://' . $server['HTTP_HOST'] . str_replace ( '/index.php' , '' , $server['PHP_SELF'] ) . '/yarm/test/lib/upload'
        $server['REQUEST_SCHEME'] . '://' . $server['HTTP_HOST'] . Url::fromRoute("yarm.test_upload")->toString()
    ;
  }

}
