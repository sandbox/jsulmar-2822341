# README.md -- yarm
20161004 jsulmar

##Overview
This module employs the MediaRecorder API (HTML5  WebRTC) to record audio using the browser and workstation microphone.

It is currently a sandbox project intended as a proof of concept, however future integration with other D8 modules (e.g. [media](https://www.drupal.org/project/media), [audiorecorderfield](https://www.drupal.org/project/audiorecorderfield)) is a possibility.


##Compatibility
Due to [MediaREcorder compatibility constraints](http://caniuse.com/#search=MediaRecorder), presently only certain desktop and Android browsers are supported (Chrome and Firefox have been tested). Plans include the addition of a Flash fallback for other desktop browsers, and mobile support is under consideration.

##Dependencies
* yarm-lib
* Compatible browser (see 'Compatibility' section)
* Local microphone
* Javascript must be enabled in the browser

##To Install

* clone the yarm sandbox module  
* clone the yarm-lib from github
* enable the module
* navigate to /yarm/test/lib

```
(from Drupal root) 
* git clone --branch 8.x-1.x jsulmar@git.drupal.org:sandbox/jsulmar/2822341.git modules/yarm
* git clone --branch master https://github.com/jsulmar/yarm-lib.git modules/yarm/yarm-lib
* drush en yarm
```

##Features
* create a recording
* playback
* download an .ogg file to the workstation
* upload an .ogg file to the host

##Known Issues
* sandbox only
* limited device support

##Recommended modules
(tbd)

##Resources
* (tbd)

## Related Modules
* [audiorecorderfield]  (https://www.drupal.org/project/audiorecorderfield)
* [media_recorder]      (https://www.drupal.org/project/media_recorder)
* [media]               (https://www.drupal.org/project/media)
* [audiorecordingfield] (https://www.drupal.org/project/audiorecordingfield)
* [audio recorder]      (https://www.drupal.org/node/1827350)
* [Zubco Audio Recorder](https://www.drupal.org/sandbox/zolexiy/1781320)
* [audioprompter]       (https://www.drupal.org/project/audioprompter)


