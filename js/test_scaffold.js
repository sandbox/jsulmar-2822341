/**
 * @file
 * Contains js for the accordion example.
 */

(function ($) {
    $(function () {
        // specify an attribute to prove basic functionality
        document.getElementById("test1-js").style.color = "blue";
        $('#test1-jQuery').css('color', 'green');
    })
})(jQuery);
