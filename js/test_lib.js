/*
 *  YarmLocalMedia
 *  20161018 jsulmar
 *
 * Instantiate the yarm-lib, specifying which player to use
 * and the URL of the ajax upload service
 */

var $=jQuery;
$(document).ready(function () {
    new YarmUi({
        playerType: 'YarmJPlayer',  //OMIT for html5 player
        uploadHandlerUrl: drupalSettings.yarm.uploadUrl     
    });
});

