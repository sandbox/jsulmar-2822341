/*
 *  YarmLocalMedia
 *  20161018 jsulmar
 *
 * Instantiate the yarm-lib, specifying which player to use
 * and the URL of the ajax upload service
 */

var $=jQuery;
$(document).ready(function () {
    
    var player= new YarmPlayer(
            '#appliances .player', 
            'YarmJPlayer',
            {
                url:        "http://jplayer.org/audio/ogg/Miaow-07-Bubble.ogg", 
                name:       "Bubble", 
                autoplay:   true
            }
    );

});

